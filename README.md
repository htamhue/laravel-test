1. Clone Repository
2. cp .env.example .env
3. Config database, mailtrap,...
4. composer install
5. php artisan migrate
6. php artisan serve
7. php artisan queue:work (Check email verify queue)
8. http://127.0.0.1:8000/products (Ajax CRUD)
9. http://127.0.0.1:8000/api/documentation (Swagger)
10. http://127.0.0.1:8000/api/products (Postman)
